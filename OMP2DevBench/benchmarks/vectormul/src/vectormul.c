/**
 * vectormul.c: This file was developed to test offload and accelerations
 * constructs on GPU from C/C++ with OpenMP 4.0 pragmas and OpenCL driver.
 *
 * Contacts: Marcio M Pereira <mpereira@ic.unicamp.br>
 *           Rafael Cardoso F Sousa <rafael.cardoso@students.ic.unicamp.br>
 *	     Luís Felipe Mattos <ra107822@students.ic.unicamp.br>
 */

#include <omp.h>
#include <stdio.h>
#include <sys/time.h>

#include "../../common/polybenchUtilFuncts.h"

//define the error threshold for the results "not matching"
#define ERROR_THRESHOLD 0.05

#define GPU_DEVICE 1
#define VECTOR_SIZE 16384

typedef float DATA_TYPE;

void compareResults(DATA_TYPE* C, DATA_TYPE* C_GPU)
{
  int i, j, fail;
  fail = 0;
	
  // Compare B and B_GPU
  for (i=1; i < VECTOR_SIZE; i++) 
    {
      if (percentDiff(C[i], C_GPU[i]) > ERROR_THRESHOLD) 
	{
	  fail++;
	}
    }
	
  // Print results
  printf("Non-Matching CPU-GPU Outputs Beyond Error Threshold of %4.2f Percent: %d\n", ERROR_THRESHOLD, fail);
	
}

int main() {
  int i;
  DATA_TYPE alpha = 2.0;
  // Allocate space for vectors A, B and C
  DATA_TYPE *A = (DATA_TYPE*)malloc(sizeof(DATA_TYPE)*VECTOR_SIZE);
  DATA_TYPE *B = (DATA_TYPE*)malloc(sizeof(DATA_TYPE)*VECTOR_SIZE);
  DATA_TYPE *C = (DATA_TYPE*)malloc(sizeof(DATA_TYPE)*VECTOR_SIZE);
  DATA_TYPE *C_GPU = (DATA_TYPE*)malloc(sizeof(DATA_TYPE)*VECTOR_SIZE);
  int n = VECTOR_SIZE;

  // Initialize vectors A & B
  for (i = 0; i < n; i++) {
    A[i] = (DATA_TYPE)i;
    B[i] = (DATA_TYPE)(i * 2);
  }

  double t_start, t_end, t_start_OMP, t_end_OMP;

  t_start_OMP = rtclock();
  #pragma omp target map(to: A[0:n], B[:n]) map(from: C_GPU[:n]) device (GPU_DEVICE)
  {
    #pragma omp parallel for
    for (i=0; i<n; ++i)
      {
	C_GPU[i] = alpha * A[i] + B[i];
      }
  }
  t_end_OMP = rtclock();
  fprintf(stdout, "GPU Runtime: %0.6lfs\n", t_end_OMP - t_start_OMP);

  t_start = rtclock();
  for (i = 0; i < n; i++)
    {
       C[i] = alpha * A[i] + B[i];
    }
  t_end = rtclock();
  fprintf(stdout, "CPU Runtime: %0.6lfs\n", t_end - t_start);//);
	
  compareResults(C, C_GPU);

  free(A);
  free(B);
  free(C);
  free(C_GPU);
  
  return 0;
}

