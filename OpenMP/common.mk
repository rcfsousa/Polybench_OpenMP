all:
	clang -fopenmp -omptargets=spir64-unknown-unknown -o ${EXECUTABLE} ${OMPFILES}
clean:
	rm -f *~ _kernel* *.ll ${EXECUTABLE}
